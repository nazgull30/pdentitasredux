# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Releases]

---

### [2.2.9] - 2023-11-03

#### Fixed

* Draw GUI in inspector Unity 2021.3.31f

---

### [2.2.8] - 2023-10-22

#### Changed

* Do not throw Exception when invoke link entity to view multiple times

---

### [2.2.7] - 2023-10-21

#### Changed

* Do not invoke Execute in CommandSystem if commands pool is empty

---

### [2.2.6] - 2023-10-21

#### Added

* CommandBuffer systems for LateUpdate

---

### [2.2.5] - 2023-09-09

#### Fixed

* Remove pointer from flag in EventDisposable

---

### [2.2.4] - 2023-09-09

#### Added

* Disposed flag pointer to EventDisposable for do not process Dispose multiple times

---

### [2.2.3] - 2023-09-05

#### Fixed

* Do not invoke component changed event by default if subscribed on remove component

---

### [2.2.2] - 2023-08-26

#### Fixed

* SlimCollector collector fixes and tests

---

### [2.2.1] - 2023-08-24

#### Added

* Use lightweight Collector version called SlimCollector for invoke entity components changed events

---

### [2.2.0] - 2023-08-21

#### Changed

* Change entity index to GenId
* Change previous entity buffer to EntityRepo where Entities stored by GenId
* Change HashSet in Groups to IntHashSet for store entities by index
* Generate Subscription methods for listen entity components change like a RX pattern

#### Removed

* Generation Listener interfaces

---

### [2.1.0] - 2021-07-13

#### Fixes

* Fixed several bugs with Entity Index code generation where index-related methods and code was not generated.

#### Changes

* Sealed attribute types EntityIndex, PrimaryEntityIndex, and CustomEntityIndex to ensure stable behavior for the
  EntityIndex code-generator.

---

### [2.0.1] - 2021-07-05

#### Fixes

* Updated Genesis package dependency min version to v2.3.2 to include several fixes for path issues

---

### [2.0.0] - 2021-06-16

#### Changed

* All code-generation plugins have been extracted from Unity as v2 Genesis .Net Core plugin assemblies (use Roslyn over
  Reflection).

## CL Template

This template should be used for any new releases to document any changes.

---

### [0.0.0] - 1900-01-01

#### Added

For new features.

#### Changed

For changes in existing functionality.

#### Deprecated

For soon-to-be removed features.

#### Removed

for now removed features.

#### Fixed

For any bug fixes.

#### Security

In case of vulnerabilities.
