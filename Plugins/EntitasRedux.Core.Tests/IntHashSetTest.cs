using System.Linq;
using FluentAssertions;
using JCMG.EntitasRedux;
using Xunit;

namespace EntitasRedux.Core.Tests
{
	public class IntHashSetTest
	{
		public static readonly TheoryDataArray<AddValueCase> AddDataMember = new(
			new AddValueCase
			{
				Indexes = new[] { 1, 16, 32, 48, 64, 80, 96, 112, 128 },
				ExpectedCount = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
				ExpectedResult = Enumerable.Repeat(true, 9).ToArray()
			},
			new AddValueCase
			{
				Indexes = new[] { 1, 1, 16, 16, 32, 32, 48, 48, 64, 64, 80, 80, 96, 96, 112, 112, 128, 128 },
				ExpectedCount = new[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9 },
				ExpectedResult = Enumerable.Range(0, 18).Select(s => s % 2 == 0).ToArray()
			}
		);

		[Theory]
		[MemberData(nameof(AddDataMember))]
		public void Add(AddValueCase valueCase)
		{
			var intHashSet = new IntHashSet();

			for (var i = 0; i < valueCase.Indexes.Length; i++)
			{
				var index = valueCase.Indexes[i];
				var count = valueCase.ExpectedCount[i];
				var result = valueCase.ExpectedResult[i];

				intHashSet.Add(index).Should().Be(result);
				intHashSet.Count.Should().Be(count);
			}
		}

		[Fact]
		public void Add_Same_Value()
		{
			var intHashSet = new IntHashSet();
			intHashSet.Add(1).Should().BeTrue();
			intHashSet.Add(1).Should().BeFalse();
		}

		public static readonly TheoryDataArray<RemoveValueCase> RemoveDataMember = new(
			new RemoveValueCase
			{
				InitValues = new[] { 1, 16, 32, 48, 64, 80, 96, 112, 128 },
				Indexes = new[] { 1, 16, 32, 48, 64, 80, 96, 112, 128 },
				ExpectedCount = new[] { 8, 7, 6, 5, 4, 3, 2, 1, 0 },
				ExpectedResult = Enumerable.Repeat(true, 9).ToArray()
			},
			new RemoveValueCase
			{
				InitValues = new[] { 1, 16, 32, 48, 64, 80, 96, 112, 128 },
				Indexes = new[] { 1, 1, 16, 16, 32, 32, 48, 48, 64, 64, 80, 80, 96, 96, 112, 112, 128, 128 },
				ExpectedCount = new[] { 8, 8, 7, 7, 6, 6, 5, 5, 4, 4, 3, 3, 2, 2, 1, 1, 0, 0 },
				ExpectedResult = Enumerable.Range(0, 18).Select(s => s % 2 == 0).ToArray()
			}
		);

		[Theory]
		[MemberData(nameof(RemoveDataMember))]
		public void Remove(RemoveValueCase valueCase)
		{
			var intHashSet = new IntHashSet(valueCase.InitValues);

			for (var i = 0; i < valueCase.Indexes.Length; i++)
			{
				var index = valueCase.Indexes[i];
				var count = valueCase.ExpectedCount[i];
				var result = valueCase.ExpectedResult[i];

				intHashSet.Remove(index).Should().Be(result);
				intHashSet.Count.Should().Be(count);
			}
		}

		[Fact]
		public void Remove_Same_Value()
		{
			var intHashSet = new IntHashSet();
			intHashSet.Add(1).Should().BeTrue();
			intHashSet.Remove(1).Should().BeTrue();
			intHashSet.Remove(1).Should().BeFalse();
		}

		[Fact]
		public void AddLargeValue_ToIntHashSetWithSmallSize_NotThrowException()
		{
			var intHashSet = new IntHashSet(1);
			intHashSet.Add(1000000).Should().BeTrue();
		}

		[Fact]
		public void AfterClear_IntHashSetCount_Zero()
		{
			var array = Enumerable.Range(1, 128).ToArray();
			var intHashSet = new IntHashSet(array);
			intHashSet.Count.Should().Be(array.Length);
			intHashSet.Clear();
			intHashSet.Count.Should().Be(0);
		}

		[Fact]
		public void AddZeroToHashSet_First_IsZero()
		{
			var intHashSet = new IntHashSet();
			intHashSet.Add(0).Should().BeTrue();
			intHashSet.First().Should().Be(0);
			intHashSet.Remove(0).Should().BeTrue();
			intHashSet.Count.Should().Be(0);
		}

		[Fact(Timeout = 2000)]
		public void Iterate()
		{
			var collection = Enumerable.Range(0, 1000000).ToArray();
			var intHashSet = new IntHashSet(collection);
			var index = 0;
			foreach (var v in intHashSet)
			{
				v.Should().Be(collection[index++]);
			}
		}


		public class RemoveValueCase
		{
			public int[] InitValues;
			public int[] Indexes;
			public int[] ExpectedCount;
			public bool[] ExpectedResult;
		}

		public class AddValueCase
		{
			public int[] Indexes;
			public int[] ExpectedCount;
			public bool[] ExpectedResult;
		}

		public class TheoryDataArray<T> : TheoryData<T>
		{
			public TheoryDataArray(params T[] valueCases)
			{
				foreach (var valueCase in valueCases)
					Add(valueCase);
			}
		}
	}
}