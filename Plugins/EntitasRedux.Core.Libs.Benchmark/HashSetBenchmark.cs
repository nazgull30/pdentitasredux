using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;
using JCMG.EntitasRedux;

namespace EntitasRedux.Core.Libs.Benchmark
{
	public class HashSetBenchmark
	{
		private static readonly int[] ValuesCollection = Enumerable.Repeat(0, 1000000).ToArray();

		[BenchmarkCategory("HashSet")]
		public class Create
		{
			[Benchmark]
			public void Create_Standard()
			{
				var hashSet = new HashSet<int>();
			}

			[Benchmark]
			public void Create_Optimized()
			{
				var intHashSet = new IntHashSet();
			}
		}

		[BenchmarkCategory("HashSet")]
		public class CreateWithInitCollection
		{
			[Benchmark]
			public void CreateWithInitCollection_Standard()
			{
				var hashSet = new HashSet<int>(ValuesCollection);
			}

			[Benchmark]
			public void CreateWithInitCollection_Optimized()
			{
				var intHashSet = new IntHashSet(ValuesCollection);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Add
		{
			private HashSet<int> _hashSet;
			private IntHashSet _intHashSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>();
				_intHashSet = new IntHashSet();
			}

			[Benchmark]
			public void Add_Standard()
			{
				for (var i = 0; i < 1000000; i++)
					_hashSet.Add(i);
			}

			[Benchmark]
			public void Add_Optimized()
			{
				for (var i = 0; i < 1000000; i++)
					_intHashSet.Add(i);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Remove
		{
			private HashSet<int> _hashSet;
			private IntHashSet _intHashSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_intHashSet = new IntHashSet(ValuesCollection);
			}

			[Benchmark]
			public void Remove_Standard()
			{
				foreach (var value in ValuesCollection)
					_hashSet.Remove(value);
			}

			[Benchmark]
			public void Remove_Optimized()
			{
				foreach (var value in ValuesCollection)
					_intHashSet.Remove(value);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Has
		{
			private HashSet<int> _hashSet;
			private IntHashSet _intHashSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_intHashSet = new IntHashSet(ValuesCollection);
			}

			[Benchmark]
			public void Has_Standard()
			{
				foreach (var value in ValuesCollection)
					_hashSet.Contains(value);
			}

			[Benchmark]
			public void Has_Optimized()
			{
				foreach (var value in ValuesCollection)
					_intHashSet.Contains(value);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Iteration
		{
			private HashSet<int> _hashSet;
			private IntHashSet _intHashSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_intHashSet = new IntHashSet(ValuesCollection);
			}

			[Benchmark]
			public void Iterate_Standard()
			{
				for (int i = 0; i < 1000000; i++)
				{
					foreach (var value in _hashSet)
					{
					}
				}
			}

			[Benchmark]
			public void Iterate_Optimized()
			{
				for (int i = 0; i < 1000000; i++)
				{
					foreach (var value in _intHashSet)
					{
					}
				}
			}
		}

		[BenchmarkCategory("HashSet")]
		public class IterationFirstAndLast
		{
			private HashSet<int> _hashSet;
			private IntHashSet _intHashSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_hashSet.Clear();
				_hashSet.Add(ValuesCollection[0]);
				_hashSet.Add(ValuesCollection[^1]);

				_intHashSet = new IntHashSet(ValuesCollection);
				_intHashSet.Clear();
				_intHashSet.Add(ValuesCollection[0]);
				_intHashSet.Add(ValuesCollection[^1]);
			}

			[Benchmark]
			public void IterateFirstAndLast_Standard()
			{
				for (int i = 0; i < 10000000; i++)
				{
					foreach (var value in _hashSet)
					{
					}
				}
			}

			[Benchmark]
			public void IterateFirstAndLast_Optimized()
			{
				for (int i = 0; i < 10000000; i++)
				{
					foreach (var value in _intHashSet)
					{
					}
				}
			}
		}
	}
}