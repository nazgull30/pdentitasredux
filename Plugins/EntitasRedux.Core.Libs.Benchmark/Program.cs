﻿using BenchmarkDotNet.Running;

namespace EntitasRedux.Core.Libs.Benchmark
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			BenchmarkRunner.Run(new[]
				{
					typeof(HashSetBenchmark.Create),
					typeof(HashSetBenchmark.CreateWithInitCollection),
					typeof(HashSetBenchmark.Add),
					typeof(HashSetBenchmark.Remove),
					typeof(HashSetBenchmark.Has),
					typeof(HashSetBenchmark.Iteration),
					typeof(HashSetBenchmark.IterationFirstAndLast),
				}
			);
		}
	}
}