using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace JCMG.EntitasRedux
{
	public class SlimCollector<TEntity> : ICollector<TEntity>
		where TEntity : class, IEntity
	{
		private readonly IContext<TEntity> _context;
		private readonly EventType _eventType;
		private readonly int _componentIndex;
		private readonly IntHashSet _collectedEntities = new();

		public SlimCollector(
			IContext<TEntity> context,
			int componentIndex,
			EventType eventType = EventType.Added
		)
		{
			_context = context;
			_componentIndex = componentIndex;
			_eventType = eventType;

			Activate();
		}

		public void Activate()
		{
			switch (_eventType)
			{
				case EventType.Added:
					_context.OnEntityCreated += OnEntityCreatedAddEvent;
					break;
				case EventType.Removed:
					_context.OnEntityCreated += OnEntityCreatedRemoveEvent;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public void Deactivate()
		{
			switch (_eventType)
			{
				case EventType.Added:
					_context.OnEntityCreated -= OnEntityCreatedAddEvent;
					break;
				case EventType.Removed:
					_context.OnEntityCreated -= OnEntityCreatedRemoveEvent;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			ClearCollectedEntities();
		}

		public int Count => _collectedEntities.Count;

		public void ClearCollectedEntities() => _collectedEntities.Clear();

		public IEnumerable<IEntity> GetEntities() => this;

		private void OnEntityCreatedAddEvent(IContext context, IEntity entity)
		{
			entity.OnComponentAdded += OnComponentAdded;
			entity.OnComponentReplaced += OnComponentReplaced;
			entity.OnDestroyEntity += OnDestroyEntity;
		}

		private void OnEntityCreatedRemoveEvent(IContext context, IEntity entity)
		{
			entity.OnComponentRemoved += OnComponentRemoved;
			entity.OnDestroyEntity += OnDestroyEntity;
		}

		private void OnDestroyEntity(IEntity e)
		{
			var entity = e as TEntity;
			if (!_collectedEntities.Remove(entity.Id.Index))
				return;

			switch (_eventType)
			{
				case EventType.Added:
					entity.OnComponentAdded -= OnComponentAdded;
					entity.OnComponentReplaced -= OnComponentReplaced;
					break;
				case EventType.Removed:
					entity.OnComponentRemoved -= OnComponentRemoved;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			entity.OnDestroyEntity -= OnDestroyEntity;
		}

		private void OnComponentAdded(IEntity entity, int index, IComponent component)
		{
			if (_componentIndex != index)
				return;

			_collectedEntities.Add(entity.Id.Index);
		}

		private void OnComponentReplaced(
			IEntity entity,
			int index,
			IComponent previousComponent,
			IComponent newComponent
		)
		{
			if (_componentIndex != index)
				return;

			_collectedEntities.Add(entity.Id.Index);
		}

		private void OnComponentRemoved(IEntity entity, int index, IComponent component)
		{
			if (_componentIndex != index)
				return;

			_collectedEntities.Add(entity.Id.Index);
		}

		public IEnumerator<TEntity> GetEnumerator() => _collectedEntities.Count > 0
			? new EntityByIndexEnumerator<TEntity>(_collectedEntities, _context)
			: Enumerable.Empty<TEntity>().GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}