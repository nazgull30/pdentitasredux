using System;

namespace JCMG.EntitasRedux.Events
{
	public struct EventDisposable<TEventDelegate> : IDisposable
		where TEventDelegate : Delegate
	{
		private readonly int _creationIndex;
		private readonly TEventDelegate _event;
		private readonly Action<int, TEventDelegate> _unsubscribe;

		private bool _disposed;

		public EventDisposable(int creationIndex, TEventDelegate @event, Action<int, TEventDelegate> unsubscribe)
		{
			_creationIndex = creationIndex;
			_event = @event;
			_unsubscribe = unsubscribe;
			_disposed = false;
		}

		public void Dispose()
		{
			if (_disposed)
				return;

			_disposed = true;
			_unsubscribe(_creationIndex, _event);
		}
	}
}