﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using EntitasRedux.Core.Libs;

namespace JCMG.EntitasRedux
{
	/// <summary>
	/// A context manages the lifecycle of entities and groups.
	/// You can create and destroy entities and get groups of entities.
	/// The preferred way to create a context is to use the generated methods
	/// from the code generator, e.g. var context = new GameContext();
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public class Context<TEntity> : IContext<TEntity>
		where TEntity : class, IEntity
	{
		private readonly Func<IEntity, IAERC> _aercFactory;
		private readonly EntityComponentReplaced _cachedComponentReplaced;
		private readonly EntityEvent _cachedDestroyEntity;

		// Cache delegates to avoid gc allocations
		private readonly EntityComponentChanged _cachedEntityChanged;
		private readonly EntityEvent _cachedEntityReleased;

		private readonly Stack<IComponent>[] _componentPools;
		private readonly ContextInfo _contextInfo;

		private readonly Dictionary<int, IEntityIndex> _entityIndices;
		private readonly ObjectPool<List<GroupChanged<TEntity>>> _groupChangedListPool;

		private readonly Dictionary<IMatcher<TEntity>, IGroup<TEntity>> _groups = new();

		private readonly List<IGroup<TEntity>>[] _groupsForIndex;

		private readonly IntHashSet _retainedEntities = new();

		private readonly int _totalComponents;

		private int _creationIndex;

		private readonly EntityRepo<TEntity> _entityRepo;

		private TEntity[] _entitiesCache;

		/// <summary>
		/// The preferred way to create a context is to use the generated methods
		/// from the code generator, e.g. var context = new GameContext();
		/// </summary>
		/// <param name="totalComponents"></param>
		/// <param name="entityFactory"></param>
		public Context(int totalComponents, Func<GenId, TEntity> entityFactory) : this(
			totalComponents,
			0,
			null,
			null,
			entityFactory)
		{
		}

		/// <summary>
		/// The preferred way to create a context is to use the generated methods
		/// from the code generator, e.g. var context = new GameContext();
		/// </summary>
		/// <param name="totalComponents"></param>
		/// <param name="startCreationIndex"></param>
		/// <param name="contextInfo"></param>
		/// <param name="aercFactory"></param>
		/// <param name="entityFactory"></param>
		public Context(
			int totalComponents,
			int startCreationIndex,
			ContextInfo contextInfo,
			Func<IEntity, IAERC> aercFactory,
			Func<GenId, TEntity> entityFactory
		)
		{
			_totalComponents = totalComponents;
			_creationIndex = startCreationIndex;

			if (contextInfo != null)
			{
				_contextInfo = contextInfo;
				if (contextInfo.componentNames.Length != totalComponents)
				{
					throw new ContextInfoException(this, contextInfo);
				}
			}
			else
			{
				_contextInfo = CreateDefaultContextInfo();
			}

			_aercFactory = aercFactory ?? (entity => new SafeAERC(entity));
			_entityRepo = new EntityRepo<TEntity>(entityFactory);

			_groupsForIndex = new List<IGroup<TEntity>>[totalComponents];
			_componentPools = new Stack<IComponent>[totalComponents];
			_entityIndices = new Dictionary<int, IEntityIndex>();
			_groupChangedListPool = new ObjectPool<List<GroupChanged<TEntity>>>(
				() => new List<GroupChanged<TEntity>>(),
				list => list.Clear());

			// Cache delegates to avoid gc allocations
			_cachedEntityChanged = UpdateGroupsComponentAddedOrRemoved;
			_cachedComponentReplaced = UpdateGroupsComponentReplaced;
			_cachedEntityReleased = OnEntityReleased;
			_cachedDestroyEntity = OnDestroyEntity;
		}

		private ContextInfo CreateDefaultContextInfo()
		{
			var componentNames = new string[_totalComponents];
			const string prefix = "Index ";
			for (var i = 0; i < componentNames.Length; i++)
			{
				componentNames[i] = prefix + i;
			}

			return new ContextInfo("Unnamed Context", componentNames, null);
		}

		public override string ToString() => _contextInfo.name;

		private void UpdateGroupsComponentAddedOrRemoved(IEntity entity, int index, IComponent component)
		{
			var groups = _groupsForIndex[index];
			if (groups == null)
				return;

			var events = _groupChangedListPool.Get();
			var tEntity = (TEntity)entity;

			for (var i = 0; i < groups.Count; i++)
			{
				events.Add(groups[i].HandleEntity(tEntity));
			}

			for (var i = 0; i < events.Count; i++)
			{
				var groupChangedEvent = events[i];
				groupChangedEvent?.Invoke(
					groups[i],
					tEntity,
					index,
					component);
			}

			_groupChangedListPool.Push(events);
		}

		private void UpdateGroupsComponentReplaced(
			IEntity entity,
			int index,
			IComponent previousComponent,
			IComponent newComponent
		)
		{
			var groups = _groupsForIndex[index];
			if (groups != null)
			{
				var tEntity = (TEntity)entity;

				for (var i = 0; i < groups.Count; i++)
				{
					groups[i]
						.UpdateEntity(
							tEntity,
							index,
							previousComponent,
							newComponent);
				}
			}
		}

		private void OnEntityReleased(IEntity entity)
		{
			if (entity.IsEnabled)
			{
				throw new EntityIsNotDestroyedException("Cannot release " + entity + "!");
			}

			var tEntity = (TEntity)entity;
			entity.RemoveAllOnEntityReleasedHandlers();
			_retainedEntities.Remove(tEntity.Id.Index);

			if (!_entityRepo.Release(tEntity.Id))
				throw new ContextDoesNotContainEntityException(
					"'" + this + "' cannot destroy " + tEntity + "!",
					"This cannot happen!?!");
		}

		private void OnDestroyEntity(IEntity entity)
		{
			var tEntity = (TEntity)entity;

			_entitiesCache = null;

			OnBeforeEntityDestroyed?.Invoke(this, tEntity);

			tEntity.InternalDestroy();

			OnEntityDestroyed?.Invoke(this, tEntity);

			if (tEntity.RetainCount > 1)
				_retainedEntities.Add(tEntity.Id.Index);

			tEntity.Release(this);
		}

		/// <summary>
		/// Occurs when an entity gets created.
		/// </summary>
		public event ContextEntityChanged OnEntityCreated;

		/// <summary>
		/// Occurs when an entity will be destroyed.
		/// </summary>
		public event ContextEntityChanged OnBeforeEntityDestroyed;

		/// <summary>
		/// Occurs when an entity got destroyed.
		/// </summary>
		public event ContextEntityChanged OnEntityDestroyed;

		/// <summary>
		/// Occurs when a group gets created for the first time.
		/// </summary>
		public event ContextGroupChanged OnGroupCreated;

		/// <summary>
		/// The total amount of components an entity can possibly have.
		/// This value is generated by the code generator, e.g ComponentLookup.TotalComponents.
		/// </summary>
		public int TotalComponents => _totalComponents;

		/// <summary>
		/// Returns all componentPools. componentPools is used to reuse
		/// removed components.
		/// Removed components will be pushed to the componentPool.
		/// Use entity.CreateComponent(index, type) to get a new or reusable
		/// component from the componentPool.
		/// </summary>
		public Stack<IComponent>[] ComponentPools => _componentPools;

		/// <summary>
		/// The contextInfo contains information about the context.
		/// It's used to provide better error messages.
		/// </summary>
		public ContextInfo ContextInfo => _contextInfo;

		/// <summary>
		/// Returns the number of entities in the context.
		/// </summary>
		public int Count => _entityRepo.ActiveCount;

		/// <summary>
		/// Returns the number of entities in the internal ObjectPool
		/// for entities which can be reused.
		/// </summary>
		public int ReusableEntitiesCount => _entityRepo.ReusableCount;

		/// <summary>
		/// Returns the number of entities that are currently retained by
		/// other objects (e.g. Group, Collector, ReactiveSystem).
		/// </summary>
		public int RetainedEntitiesCount => _retainedEntities.Count;

		/// <summary>
		/// Creates a new entity or gets a reusable entity from the
		/// internal ObjectPool for entities.
		/// </summary>
		/// <returns></returns>
		public TEntity CreateEntity()
		{
			TEntity entity;

			if (_entityRepo.ReusableCount > 0)
			{
				entity = _entityRepo.Alloc();
				entity.Reactivate(_creationIndex++);
			}
			else
			{
				entity = _entityRepo.Alloc();
				entity.Initialize(
					_creationIndex++,
					_totalComponents,
					_componentPools,
					_contextInfo,
					_aercFactory(entity)
				);
			}

			entity.Retain(this);
			_entitiesCache = null;

			entity.OnComponentAdded += _cachedEntityChanged;
			entity.OnComponentRemoved += _cachedEntityChanged;
			entity.OnComponentReplaced += _cachedComponentReplaced;
			entity.OnEntityReleased += _cachedEntityReleased;
			entity.OnDestroyEntity += _cachedDestroyEntity;

			OnEntityCreated?.Invoke(this, entity);

			return entity;
		}

		/// <summary>
		/// Destroys all entities in the context.
		/// Throws an exception if there are still retained entities.
		/// </summary>
		public void DestroyAllEntities()
		{
			_entityRepo.ReleaseAll();

			if (_retainedEntities.Count == 0)
				return;

			var retainedList = new List<IEntity>();
			foreach (var index in _retainedEntities)
			{
				var entity = _entityRepo.GetByIndex(index);
				retainedList.Add(entity);
			}

			throw new ContextStillHasRetainedEntitiesException(this, retainedList.ToArray());
		}

		/// <summary>
		/// Determines whether the context has the specified entity.
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public bool HasEntity(TEntity entity) => _entityRepo.Has(entity.Id);

		/// <summary>
		/// Returns all entities which are currently in the context.
		/// </summary>
		/// <returns></returns>
		public TEntity[] GetEntities()
		{
			var entities = new TEntity[_entityRepo.ActiveCount];
			var index = 0;

			foreach (var entity in _entityRepo)
				entities[index++] = entity;

			return entities;
		}

		public void GetEntities(List<TEntity> buffer)
		{
			foreach (var entity in _entityRepo)
				buffer.Add(entity);
		}

		public TEntity GetEntity(int index) => _entityRepo.GetByIndex(index);

		public TEntity GetEntity(GenId id) => _entityRepo.GetById(id);

		/// <summary>
		/// Returns a group for the specified matcher.
		/// Calling context.GetGroup(matcher) with the same matcher will always
		/// return the same instance of the group.
		/// </summary>
		/// <param name="matcher"></param>
		/// <returns></returns>
		public IGroup<TEntity> GetGroup(IMatcher<TEntity> matcher)
		{
			if (_groups.TryGetValue(matcher, out var group))
				return group;

			group = new Group<TEntity>(matcher, this);
			foreach (var entity in _entityRepo)
				group.HandleEntitySilently(entity);

			_groups.Add(matcher, group);

			for (var i = 0; i < matcher.Indices.Length; i++)
			{
				var index = matcher.Indices[i];
				_groupsForIndex[index] ??= new List<IGroup<TEntity>>();
				_groupsForIndex[index].Add(group);
			}

			OnGroupCreated?.Invoke(this, group);
			return group;
		}

		/// <summary>
		/// Adds the IEntityIndex for the specified id.
		/// There can only be one IEntityIndex per id.
		/// </summary>
		/// <param name="entityIndex"></param>
		public void AddEntityIndex(IEntityIndex entityIndex)
		{
			if (_entityIndices.ContainsKey(entityIndex.Id))
			{
				throw new ContextEntityIndexDoesAlreadyExistException(this, entityIndex.Id);
			}

			_entityIndices.Add(entityIndex.Id, entityIndex);
		}

		/// <summary>
		/// Gets the IEntityIndex for the specified id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public IEntityIndex GetEntityIndex(int id)
		{
			if (!_entityIndices.TryGetValue(id, out var entityIndex))
			{
				throw new ContextEntityIndexDoesNotExistException(this, id);
			}

			return entityIndex;
		}

		/// <summary>
		/// Resets the creationIndex back to 0.
		/// </summary>
		public void ResetCreationIndex()
		{
			_creationIndex = 0;
		}

		/// <summary>
		/// Clears the componentPool at the specified index.
		/// </summary>
		/// <param name="index"></param>
		public void ClearComponentPool(int index)
		{
			var componentPool = _componentPools[index];
			componentPool?.Clear();
		}

		/// <summary>
		/// Clears all componentPools.
		/// </summary>
		public void ClearComponentPools()
		{
			for (var i = 0; i < _componentPools.Length; i++)
			{
				ClearComponentPool(i);
			}
		}

		/// <summary>
		/// Resets the context (destroys all entities and
		/// resets creationIndex back to 0).
		/// </summary>
		public void Reset()
		{
			DestroyAllEntities();
			ResetCreationIndex();
		}

		/// <summary>
		/// Removes all event handlers
		/// OnEntityCreated, OnEntityWillBeDestroyed,
		/// OnEntityDestroyed and OnGroupCreated
		/// </summary>
		public void RemoveAllEventHandlers()
		{
			OnEntityCreated = null;
			OnBeforeEntityDestroyed = null;
			OnEntityDestroyed = null;
			OnGroupCreated = null;
		}
	}
}