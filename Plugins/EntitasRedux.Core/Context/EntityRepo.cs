using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Libs;

namespace JCMG.EntitasRedux
{
	public class EntityRepo<TEntity> : IEnumerable<TEntity>
		where TEntity : class, IEntity
	{
		private readonly Func<GenId, TEntity> _entityFactory;
		private readonly IntHashSet _reusableIndex = new();

		private TEntity[] _pool = new TEntity[8];
		private int _allocIndex;
		private int _version;

		public int ActiveCount => _allocIndex - _reusableIndex.Count;

		public int ReusableCount => _reusableIndex.Count;

		public EntityRepo(Func<GenId, TEntity> entityFactory)
		{
			_entityFactory = entityFactory;
		}

		public TEntity Alloc()
		{
			_version++;

			if (_reusableIndex.Count > 0)
			{
				var index = _reusableIndex.First();
				_reusableIndex.Remove(index);
				return _pool[index];
			}

			if (_allocIndex >= _pool.Length)
				Array.Resize(ref _pool, _pool.Length << 1);

			var entity = _entityFactory(new GenId(_allocIndex));
			_pool[_allocIndex] = entity;
			_allocIndex++;
			return entity;
		}

		public bool Has(GenId id)
		{
			if (_reusableIndex.Contains(id.Index))
				return false;

			var entity = _pool[id.Index];
			return entity.Id == id;
		}

		public bool Release(GenId id)
		{
			_version++;
			if (_reusableIndex.Contains(id.Index))
				return false;

			var entity = _pool[id.Index];
			if (entity.Id != id)
				return false;

			_reusableIndex.Add(id.Index);
			return true;
		}

		public void ReleaseAll()
		{
			_version++;
			for (var i = 0; i < _allocIndex; i++)
			{
				if (_reusableIndex.Contains(i))
					continue;

				var entity = _pool[i];
				entity.Destroy();
			}
		}

		public TEntity GetById(GenId id)
		{
			var entity = _pool[id.Index];
			if (entity.Id != id || !entity.IsEnabled)
				return null;

			return entity;
		}

		public TEntity GetByIndex(int index) => index < _allocIndex ? _pool[index] : null;

		public IEnumerator<TEntity> GetEnumerator() => new Enumarator(this);

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		private class Enumarator : IEnumerator<TEntity>
		{
			private readonly EntityRepo<TEntity> _entityRepo;
			private readonly int _version;

			private TEntity _entity;
			private int _index = -1;

			public Enumarator(EntityRepo<TEntity> entityRepo)
			{
				_entityRepo = entityRepo;
				_version = entityRepo._version;
			}

			public TEntity Current => _entity;
			object IEnumerator.Current => Current;

			public bool MoveNext()
			{
				if (_version != _entityRepo._version)
					throw new InvalidOperationException("Collection is modified");

				while (++_index < _entityRepo._allocIndex)
				{
					if (_entityRepo._reusableIndex.Contains(_index))
						continue;

					_entity = _entityRepo._pool[_index];
					return true;
				}

				return false;
			}

			public void Reset()
			{
				if (_version != _entityRepo._version)
					throw new InvalidOperationException("Collection is modified");

				_index = -1;
			}

			public void Dispose()
			{
			}
		}
	}
}