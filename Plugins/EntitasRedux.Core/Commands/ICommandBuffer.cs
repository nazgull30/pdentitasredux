using System;

namespace JCMG.EntitasRedux.Commands
{
	public interface ICommandBuffer
	{
		ref TCommand Create<TCommand>() where TCommand : struct;

		void Create<TCommand>(TCommand command) where TCommand : struct;

		Span<TCommand> GetCommands<TCommand>() where TCommand : struct;

		void Clear<TCommand>() where TCommand : struct;
	}
}