using System;

namespace JCMG.EntitasRedux
{
	public abstract class ComponentReplacedReactiveSystem<TEntity, TComponent, TValue> : IReactiveSystem, IUpdateSystem
		where TEntity : class, IEntity
		where TComponent : class, IComponent
	{
		private readonly IContext<TEntity> _context;
		private readonly ComponentReplacedCollector<TEntity, TComponent, TValue> _collector;

		protected ComponentReplacedReactiveSystem(IContext<TEntity> context)
		{
			_context = context;
			var componentIndex = FindComponentIndex(typeof(TComponent));
			_collector = new ComponentReplacedCollector<TEntity, TComponent, TValue>(
				context,
				componentIndex,
				GetValue,
				Default
			);
		}

		protected virtual TValue Default => default;

		public void Activate() => _collector.Activate();

		public void Deactivate() => _collector.Deactivate();

		public void Clear() => _collector.ClearCollectedEntities();

		public void Update()
		{
			if (_collector.Count == 0)
				return;

			var entries = _collector.GetCollectedEntities();
			foreach (var entry in entries)
			{
				var replaceEvent = entry.Value;
				var entity = _context.GetEntity(entry.Key);
				OnReplaced(entity, replaceEvent.Last, replaceEvent.Next);
			}

			_collector.ClearCollectedEntities();
		}

		protected abstract Type[] GetComponentTypes();

		protected abstract TValue GetValue(TComponent component);

		protected abstract void OnReplaced(TEntity entity, TValue last, TValue next);

		private int FindComponentIndex(Type type)
		{
			var componentTypes = GetComponentTypes();
			for (var i = 0; i < componentTypes.Length; i++)
			{
				var componentType = componentTypes[i];
				if (componentType == type)
					return i;
			}

			throw new Exception($"[{GetType().Name}] Cannot find component index {type.Name}");
		}
	}
}