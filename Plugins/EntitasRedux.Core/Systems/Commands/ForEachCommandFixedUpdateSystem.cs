using System;

namespace JCMG.EntitasRedux.Commands
{
	public abstract class ForEachCommandFixedUpdateSystem<TCommand> : CommandFixedUpdateSystem<TCommand>, IUpdateSystem
		where TCommand : struct
	{
		protected ForEachCommandFixedUpdateSystem(ICommandBuffer commandBuffer) : base(commandBuffer)
		{
		}

		public void Update() => ExecuteCommands();

		protected sealed override void Execute(Span<TCommand> commands)
		{
			foreach (ref var command in commands)
				Execute(ref command);
		}

		protected abstract void Execute(ref TCommand command);
	}
}