using System;

namespace JCMG.EntitasRedux.Commands
{
	public abstract class ForEachCommandUpdateSystem<TCommand> : CommandSystem<TCommand>, IUpdateSystem
		where TCommand : struct
	{
		protected ForEachCommandUpdateSystem(ICommandBuffer commandBuffer) : base(commandBuffer)
		{
		}

		public void Update() => ExecuteCommands();

		protected sealed override void Execute(Span<TCommand> commands)
		{
			foreach (ref var command in commands)
				Execute(ref command);
		}

		protected abstract void Execute(ref TCommand command);
	}
}