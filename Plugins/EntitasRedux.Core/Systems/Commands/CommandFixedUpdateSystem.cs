namespace JCMG.EntitasRedux.Commands
{
	public abstract class CommandFixedUpdateSystem<TCommand> : CommandSystem<TCommand>, IFixedUpdateSystem
		where TCommand : struct
	{
		protected CommandFixedUpdateSystem(ICommandBuffer commandBuffer) : base(commandBuffer)
		{
		}

		public void FixedUpdate() => ExecuteCommands();
	}
}
