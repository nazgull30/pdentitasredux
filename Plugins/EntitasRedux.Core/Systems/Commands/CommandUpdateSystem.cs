namespace JCMG.EntitasRedux.Commands
{
	public abstract class CommandUpdateSystem<TCommand> : CommandSystem<TCommand>, IUpdateSystem
		where TCommand : struct
	{
		protected CommandUpdateSystem(ICommandBuffer commandBuffer) : base(commandBuffer)
		{
		}

		public void Update() => ExecuteCommands();
	}
}