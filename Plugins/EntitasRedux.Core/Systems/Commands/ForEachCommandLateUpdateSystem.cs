using System;

namespace JCMG.EntitasRedux.Commands
{
	public abstract class ForEachCommandLateUpdateSystem<TCommand> : CommandLateUpdateSystem<TCommand>, IUpdateSystem
		where TCommand : struct
	{
		protected ForEachCommandLateUpdateSystem(ICommandBuffer commandBuffer) : base(commandBuffer)
		{
		}

		public void Update() => ExecuteCommands();

		protected sealed override void Execute(Span<TCommand> commands)
		{
			foreach (ref var command in commands)
				Execute(ref command);
		}

		protected abstract void Execute(ref TCommand command);
	}
}