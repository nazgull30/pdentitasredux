using System;

namespace JCMG.EntitasRedux.Commands
{
	public abstract class CommandSystem<TCommand> : ISystem
		where TCommand : struct
	{
		private readonly ICommandBuffer _commandBuffer;

		protected virtual bool CleanUp => true;

		protected CommandSystem(ICommandBuffer commandBuffer)
		{
			_commandBuffer = commandBuffer;
		}

		protected void ExecuteCommands()
		{
			var commands = _commandBuffer.GetCommands<TCommand>();
			if (commands.Length == 0)
				return;

			Execute(commands);
			if (CleanUp)
				_commandBuffer.Clear<TCommand>();
		}

		protected abstract void Execute(Span<TCommand> commands);
	}
}