using System.Collections.Generic;
using UnityEngine.Pool;

namespace JCMG.EntitasRedux
{
	public abstract class FixedReactiveSystem<TEntity> : IReactiveSystem, IFixedUpdateSystem
		where TEntity : class, IEntity
	{
		private readonly IContext<TEntity> _context;
		private readonly ICollector<TEntity> _collector;
		private string _toStringCache;

		protected FixedReactiveSystem(IContext<TEntity> context)
		{
			_context = context;
			_collector = GetTrigger(context);
		}

		protected FixedReactiveSystem(ICollector<TEntity> collector, IContext<TEntity> context)
		{
			_collector = collector;
			_context = context;
		}

		protected abstract ICollector<TEntity> GetTrigger(IContext<TEntity> context);

		protected abstract bool Filter(TEntity entity);

		protected abstract void Execute(IEnumerable<TEntity> entities);

		public void Activate()
		{
			_collector.Activate();
		}

		public void Deactivate()
		{
			_collector.Deactivate();
		}

		public void Clear()
		{
			_collector.ClearCollectedEntities();
		}

		public void FixedUpdate()
		{
			if (_collector.Count == 0)
				return;

			using (ListPool<TEntity>.Get(out var buffer))
			{
				foreach (var entity in _collector)
				{
					if (!Filter(entity))
						continue;

					entity.Retain(this);
					buffer.Add(entity);
				}

				_collector.ClearCollectedEntities();
				if (buffer.Count == 0)
					return;

				Execute(buffer);

				for (var index = 0; index < buffer.Count; ++index)
					buffer[index].Release(this);
			}
		}

		public override string ToString() => _toStringCache ??= "ReactiveSystem(" + GetType().Name + ")";

		~FixedReactiveSystem()
		{
			Deactivate();
		}
	}
}