using System.Collections;
using System.Collections.Generic;

namespace JCMG.EntitasRedux
{
	public class EntityByIndexEnumerator<TEntity> : IEnumerator<TEntity>
		where TEntity : class, IEntity
	{
		private readonly IEnumerator<int> _indexEnumerator;
		private readonly IContext<TEntity> _context;

		public EntityByIndexEnumerator(IEnumerable<int> indexEnumerator, IContext<TEntity> context)
		{
			_indexEnumerator = indexEnumerator.GetEnumerator();
			_context = context;
		}

		private TEntity _entity;
		public TEntity Current => _entity;
		object IEnumerator.Current => _entity;

		public bool MoveNext()
		{
			if (!_indexEnumerator.MoveNext())
				return false;

			_entity = _context.GetEntity(_indexEnumerator.Current);
			return true;
		}

		public void Reset() => _indexEnumerator.Reset();

		public void Dispose()
		{
		}
	}
}