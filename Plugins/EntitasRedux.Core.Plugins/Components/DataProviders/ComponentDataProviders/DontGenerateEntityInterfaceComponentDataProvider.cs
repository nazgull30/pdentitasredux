using System.Linq;
using Genesis.Plugin;
using JCMG.EntitasRedux;

namespace EntitasRedux.Core.Plugins
{
	internal sealed class DontGenerateEntityInterfaceComponentDataProvider : IComponentDataProvider
	{
		public void Provide(ICachedNamedTypeSymbol cachedNamedTypeSymbol, ComponentData data)
		{
			var attrs = cachedNamedTypeSymbol.GetAttributes(nameof(DontGenerateEntityInterfaceAttribute))
				.ToArray();
			data.IsDontGenerateEntityInterface(attrs.Any());
		}
	}
}
