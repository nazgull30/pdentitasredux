using Genesis.Plugin;

namespace EntitasRedux.Core.Plugins
{
	internal sealed class IsListenerComponentDataProvider : IComponentDataProvider
	{
		public void Provide(ICachedNamedTypeSymbol cachedNamedTypeSymbol, ComponentData data)
			=> data.IsListener(false);
	}
}
