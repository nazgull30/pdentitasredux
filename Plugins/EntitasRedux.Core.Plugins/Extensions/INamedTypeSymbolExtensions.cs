﻿using System.Linq;
using Genesis.Plugin;
using JCMG.EntitasRedux;
using Microsoft.CodeAnalysis;

namespace EntitasRedux.Core.Plugins
{
	public static class INamedTypeSymbolExtensions
	{
		public static bool HasContexts(this INamedTypeSymbol namedTypeSymbol)
		{
			return namedTypeSymbol.GetContextNames().Any();
		}

		public static string[] GetContextNames(this INamedTypeSymbol namedTypeSymbol)
		{
			const string attribute = "Attribute";
			
			return namedTypeSymbol.GetAttributes()
				.Where(x => x.AttributeClass.GetBaseTypesAndThis().Any(x => x.Name == nameof(ContextAttribute)))
				.Select(attributeData =>
				{
					var className = attributeData.AttributeClass.Name;
					if (className == nameof(ContextAttribute))
						return attributeData.ConstructorArguments[0].Value.ToString();

					if (!className.EndsWith(attribute))
						return className;

					var pureClassName = className.Remove(className.Length - attribute.Length, attribute.Length);
					return string.IsNullOrEmpty(pureClassName) ? className : pureClassName;
				})
				.ToArray();
		}
	}
}