# Benchmark HashSet<int> vs IntHashSet

## Test environment

```
BenchmarkDotNet v0.13.7, Manjaro Linux
Intel Core i7-10750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK 7.0.305
[Host]     : .NET Core 3.1.32 (CoreCLR 4.700.22.55902, CoreFX 4.700.22.56512), X64 RyuJIT AVX2
DefaultJob : .NET Core 3.1.32 (CoreCLR 4.700.22.55902, CoreFX 4.700.22.56512), X64 RyuJIT AVX2
```

## Results

**Standard** - HashSet<int>
**Optimized** - IntHashSet

| Method                             |      Mean |     Error |    StdDev |
|:-----------------------------------|----------:|----------:|----------:|
| Create_Standard                    |  12.98 ns |  0.232 ns |  0.206 ns |
| Create_Optimized                   |  19.87 ns |  0.439 ns |  1.217 ns |
| ------                             |           |           |           |
| CreateWithInitCollection_Standard  | 19.408 ms | 0.2138 ms | 0.1896 ms |
| CreateWithInitCollection_Optimized |  7.288 ms | 0.0877 ms | 0.0820 ms |
| ------                             |           |           |           |
| Add_Standard                       | 35.887 ms | 0.3694 ms | 0.3274 ms |
| Add_Optimized                      |  2.663 ms | 0.0316 ms | 0.0280 ms |
| ------                             |           |           |           |
| Remove_Standard                    |  9.965 ms | 0.1939 ms | 0.1619 ms |
| Remove_Optimized                   |  2.778 ms | 0.0530 ms | 0.0651 ms |
| ------                             |           |           |           |
| Has_Standard                       | 13.269 ms | 0.2522 ms | 0.2236 ms |
| Has_Optimized                      |  1.328 ms | 0.0259 ms | 0.0216 ms |



