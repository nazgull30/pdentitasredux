using System;

namespace EntitasRedux.Core.Libs
{
	public struct GenId
	{
		public int Index;
		public int CreationIndex;

		public GenId(int index) : this()
		{
			Index = index;
		}

		public static bool operator ==(GenId a, GenId b)
			=> a.Index == b.Index && a.CreationIndex == b.CreationIndex;

		public static bool operator !=(GenId a, GenId b) => !(a == b);

		public bool Equals(GenId other) => Index == other.Index && CreationIndex == other.CreationIndex;

		public override bool Equals(object obj) => obj is GenId other && Equals(other);

		public override int GetHashCode() => HashCode.Combine(Index, CreationIndex);

		public override string ToString() => $"{nameof(Index)}: {Index}, {nameof(CreationIndex)}: {CreationIndex}";
	}
}