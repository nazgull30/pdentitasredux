using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace JCMG.EntitasRedux
{
	public sealed class IntHashSet : IEnumerable<int>
	{
		private const int BIT_SHIFT_PER_INT32 = 5;

		private int[] _buckets;
		private int _length;
		private int _version;

		public IntHashSet() : this(256)
		{
		}

		public IntHashSet(int length)
		{
			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length), "Need non zero value");

			_buckets = new int[GetInt32ArrayLengthFromBitLength(length)];
			_version = 0;
		}

		public IntHashSet(IntHashSet hashSet)
		{
			if (hashSet == null)
				throw new ArgumentNullException(nameof(hashSet));

			var bucketsSize = hashSet._buckets.Length;
			_buckets = new int[bucketsSize];
			_length = hashSet._length;

			Array.Copy(hashSet._buckets, _buckets, bucketsSize);

			_version = hashSet._version;
		}

		public IntHashSet(IEnumerable<int> collection) : this()
		{
			foreach (var key in collection)
				Add(key);
		}

		public int Count => _length;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool Contains(int key)
		{
			var bucket = key >> BIT_SHIFT_PER_INT32;
			if (bucket >= _buckets.Length)
				return false;

			return (_buckets[bucket] & (1 << key)) != 0;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool Add(int key)
		{
			_version++;

			var bucket = key >> BIT_SHIFT_PER_INT32;
			if (bucket >= _buckets.Length)
				ExpandBucketsArray(bucket);

			ref var segment = ref _buckets[bucket];
			var newSegment = segment | (1 << key);

			if (segment == newSegment)
				return false;

			segment = newSegment;
			_length++;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool Remove(int key)
		{
			_version++;

			var bucket = key >> BIT_SHIFT_PER_INT32;
			if (bucket >= _buckets.Length)
				return false;

			ref var segment = ref _buckets[bucket];
			var newSegment = segment & ~(1 << key);

			if (segment == newSegment)
				return false;

			segment = newSegment;
			_length--;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Clear()
		{
			_version++;
			_length = 0;
			Array.Clear(_buckets, 0, _buckets.Length);
		}

		private void ExpandBucketsArray(int bucket)
		{
			var desiredSize = _buckets.Length << 1;
			while (bucket >= desiredSize)
				desiredSize <<= 1;

			Array.Resize(ref _buckets, desiredSize);
		}

		public IEnumerator<int> GetEnumerator() => new Enumerator(this);

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		/// <summary>
		/// Used for conversion between different representations of bit array. 
		/// Returns (n+(div-1))/div, rearranged to avoid arithmetic overflow. 
		/// For example, in the bit to int case, the straightforward calc would 
		/// be (n+31)/32, but that would cause overflow. So instead it's 
		/// rearranged to ((n-1)/32) + 1, with special casing for 0.
		/// 
		/// Usage:
		/// GetArrayLength(77, BitsPerInt32): returns how many ints must be 
		/// allocated to store 77 bits.
		/// </summary>
		/// <param name="n"></param>
		/// <param name="div">use a conversion constant, e.g. BytesPerInt32 to get
		/// how many ints are required to store n bytes</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static int GetInt32ArrayLengthFromBitLength(int n)
			=> (int)((uint)(n - 1 + (1 << BIT_SHIFT_PER_INT32)) >> BIT_SHIFT_PER_INT32);

		private class Enumerator : IEnumerator<int>
		{
			private readonly IntHashSet _mask;
			private readonly int _version;

			private int _bucket;
			private int _index;

			public Enumerator(IntHashSet mask)
			{
				_mask = mask;
				_version = _mask._version;
			}

			private int _value;
			public int Current => _value;
			object IEnumerator.Current => _value;

			public bool MoveNext()
			{
				if (_version != _mask._version)
					throw new InvalidOperationException("InvalidOperation_EnumFailedVersion");

				while (_bucket < _mask._buckets.Length)
				{
					ref var mask = ref _mask._buckets[_bucket];
					if (mask == 0)
					{
						_bucket++;
						continue;
					}

					while (_index < 32)
					{
						if ((mask & (1 << _index)) == 0)
						{
							_index++;
							continue;
						}

						_value = 32 * _bucket + _index;
						_index++;
						return true;
					}

					_bucket++;
					_index = 0;
				}

				return false;
			}

			public void Reset()
			{
				if (_version != _mask._version)
					throw new InvalidOperationException("InvalidOperation_EnumFailedVersion");

				_bucket = 0;
				_index = 0;
				_value = -1;
			}

			public void Dispose()
			{
			}
		}
	}
}