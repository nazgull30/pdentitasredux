using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace JCMG.EntitasRedux
{
	public class IntDictionaryRef<T> : IEnumerable<IntDictionaryRef<T>.IntValuePair>
		where T : struct
	{
		private const int BIT_SHIFT_PER_INT32 = 5;

		private T[] _values;
		private int[] _buckets;
		private int _length;
		private int _version;

		public IntDictionaryRef() : this(64)
		{
		}

		public IntDictionaryRef(int length)
		{
			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length), "Need non zero value");

			_buckets = new int[GetInt32ArrayLengthFromBitLength(length)];
			_values = new T[length];
			_version = 0;
		}

		// public IntDictionaryRef(IntDictionaryRef<T> hashSet)
		// {
		// 	if (hashSet == null)
		// 		throw new ArgumentNullException(nameof(hashSet));
		//
		// 	var bucketsSize = hashSet._buckets.Length;
		// 	_buckets = new int[bucketsSize];
		// 	_length = hashSet._length;
		//
		// 	Array.Copy(hashSet._buckets, _buckets, bucketsSize);
		//
		// 	_version = hashSet._version;
		// }

		public int Count => _length;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool Contains(int key)
		{
			var bucket = key >> BIT_SHIFT_PER_INT32;
			if (bucket >= _buckets.Length)
				return false;

			return (_buckets[bucket] & (1 << key)) != 0;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool Add(int key, T value)
		{
			_version++;

			var bucket = key >> BIT_SHIFT_PER_INT32;
			if (bucket >= _buckets.Length)
				ExpandBucketsArray(bucket);

			ref var segment = ref _buckets[bucket];
			var newSegment = segment | (1 << key);

			if (segment == newSegment)
				return false;

			if (key >= _values.Length)
				ExpandValuesArray(key);

			segment = newSegment;
			_values[key] = value;
			_length++;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ref T AddOrGet(int key)
		{
			_version++;

			var bucket = key >> BIT_SHIFT_PER_INT32;
			if (bucket >= _buckets.Length)
				ExpandBucketsArray(bucket);

			ref var segment = ref _buckets[bucket];
			var newSegment = segment | (1 << key);

			if (segment == newSegment)
				return ref _values[key];

			if (key >= _values.Length)
				ExpandValuesArray(key);

			segment = newSegment;
			_length++;
			return ref _values[key];
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ref T Get(int key)
		{
			if (!Contains(key))
				throw new Exception($"[{GetType().Name}] Does not have key: {key}");

			return ref _values[key];
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool Remove(int key)
		{
			_version++;

			var bucket = key >> BIT_SHIFT_PER_INT32;
			if (bucket >= _buckets.Length)
				return false;

			ref var segment = ref _buckets[bucket];
			var newSegment = segment & ~(1 << key);

			if (segment == newSegment)
				return false;

			segment = newSegment;
			_values[key] = default;
			_length--;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Clear()
		{
			_version++;
			_length = 0;
			Array.Clear(_buckets, 0, _buckets.Length);
			Array.Clear(_values, 0, _values.Length);
		}

		private void ExpandBucketsArray(int bucket)
		{
			var desiredSize = _buckets.Length << 1;
			while (bucket >= desiredSize)
				desiredSize <<= 1;

			Array.Resize(ref _buckets, desiredSize);
		}

		private void ExpandValuesArray(int key)
		{
			var desiredSize = _values.Length << 1;
			while (key >= desiredSize)
				desiredSize <<= 1;

			Array.Resize(ref _values, desiredSize);
		}

		public IEnumerator<IntValuePair> GetEnumerator() => new Enumerator(this);

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		/// <summary>
		/// Used for conversion between different representations of bit array. 
		/// Returns (n+(div-1))/div, rearranged to avoid arithmetic overflow. 
		/// For example, in the bit to int case, the straightforward calc would 
		/// be (n+31)/32, but that would cause overflow. So instead it's 
		/// rearranged to ((n-1)/32) + 1, with special casing for 0.
		/// 
		/// Usage:
		/// GetArrayLength(77, BitsPerInt32): returns how many ints must be 
		/// allocated to store 77 bits.
		/// </summary>
		/// <param name="n"></param>
		/// <param name="div">use a conversion constant, e.g. BytesPerInt32 to get
		/// how many ints are required to store n bytes</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static int GetInt32ArrayLengthFromBitLength(int n)
			=> (int)((uint)(n - 1 + (1 << BIT_SHIFT_PER_INT32)) >> BIT_SHIFT_PER_INT32);

		private class Enumerator : IEnumerator<IntValuePair>
		{
			private readonly IntDictionaryRef<T> _dictionary;
			private readonly int _version;

			private int _bucket;
			private int _index;

			public Enumerator(IntDictionaryRef<T> dictionary)
			{
				_dictionary = dictionary;
				_version = _dictionary._version;
			}

			private IntValuePair _pair;
			public IntValuePair Current => _pair;
			object IEnumerator.Current => _pair;

			public bool MoveNext()
			{
				if (_version != _dictionary._version)
					throw new InvalidOperationException("InvalidOperation_EnumFailedVersion");

				while (_bucket < _dictionary._buckets.Length)
				{
					ref var mask = ref _dictionary._buckets[_bucket];
					if (mask == 0)
					{
						_bucket++;
						continue;
					}

					while (_index < 32)
					{
						if ((mask & (1 << _index)) == 0)
						{
							_index++;
							continue;
						}

						_pair.Key = 32 * _bucket + _index;
						_pair.Value = _dictionary._values[_pair.Key];
						_index++;
						return true;
					}

					_bucket++;
					_index = 0;
				}

				return false;
			}

			public void Reset()
			{
				if (_version != _dictionary._version)
					throw new InvalidOperationException("InvalidOperation_EnumFailedVersion");

				_bucket = 0;
				_index = 0;
				_pair = default;
			}

			public void Dispose()
			{
			}
		}

		public struct IntValuePair
		{
			public int Key;
			public T Value;
		}
	}
}