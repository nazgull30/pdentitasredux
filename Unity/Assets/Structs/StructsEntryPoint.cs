using System.Collections;
using UnityEngine;

namespace Structs
{
    public class StructsEntryPoint : MonoBehaviour
    {
        private Feature _feature;
        private VisualDebugContext _context;
        private void Awake()
        {
            _context = new VisualDebugContext();

            var entity1 = _context.CreateEntity();
            entity1.AddColor(Color.black);
            
            var entity2 = _context.CreateEntity();
            entity2.AddColor(Color.black);
            
            
            Debug.Log(_context.Count);

            _feature = new Feature();
            _feature.Add(new StructSystem(_context));
            _feature.Add(new StructReactiveSystem(_context));

            // InitEntityKeys();
            // Debug.Log(EntityKeyHelper.PoolNrValues.Length);
            
            StartCoroutine(ChangeColor(Color.white, 2));
            StartCoroutine(ChangeColor(Color.blue, 4));
            StartCoroutine(ChangeColor(Color.green, 6));
            
        }
        
        private void Update()
        {
            _feature.Update();
        }

        private IEnumerator ChangeColor(Color color, int delay)
        {
            yield return new WaitForSeconds(delay);
            var gGroup = _context.GetGroup(VisualDebugMatcher.Color);
            foreach (var entity in gGroup.GetEntities())
            {
                entity.ReplaceColor(color);   
            }
        }
    }
    
}
