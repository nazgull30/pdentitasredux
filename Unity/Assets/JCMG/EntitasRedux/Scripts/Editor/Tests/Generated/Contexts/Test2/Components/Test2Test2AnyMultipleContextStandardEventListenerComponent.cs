//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class Test2Entity
{
	public Test2AnyMultipleContextStandardEventListenerComponent Test2AnyMultipleContextStandardEventListener { get { return (Test2AnyMultipleContextStandardEventListenerComponent)GetComponent(Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener); } }
	public bool HasTest2AnyMultipleContextStandardEventListener { get { return HasComponent(Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener); } }

	public void AddTest2AnyMultipleContextStandardEventListener()
	{
		var index = Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener;
		var component = (Test2AnyMultipleContextStandardEventListenerComponent)CreateComponent(index, typeof(Test2AnyMultipleContextStandardEventListenerComponent));
		AddComponent(index, component);
	}

	public void ReplaceTest2AnyMultipleContextStandardEventListener()
	{
		ReplaceComponent(Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener, Test2AnyMultipleContextStandardEventListener);
	}

	public void RemoveTest2AnyMultipleContextStandardEventListener()
	{
		RemoveComponent(Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener);
	}
}

//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class Test2Matcher
{
	static JCMG.EntitasRedux.IMatcher<Test2Entity> _matcherTest2AnyMultipleContextStandardEventListener;

	public static JCMG.EntitasRedux.IMatcher<Test2Entity> Test2AnyMultipleContextStandardEventListener
	{
		get
		{
			if (_matcherTest2AnyMultipleContextStandardEventListener == null)
			{
				var matcher = (JCMG.EntitasRedux.Matcher<Test2Entity>)JCMG.EntitasRedux.Matcher<Test2Entity>.AllOf(Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener);
				matcher.ComponentNames = Test2ComponentsLookup.ComponentNames;
				_matcherTest2AnyMultipleContextStandardEventListener = matcher;
			}

			return _matcherTest2AnyMultipleContextStandardEventListener;
		}
	}
}

//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class Test2Entity
{
	public System.IDisposable SubscribeAnyMultipleContextStandardEvent(OnTest2AnyMultipleContextStandardEvent value, bool invokeOnSubscribe = true)
	{
		var componentListener = HasTest2AnyMultipleContextStandardEventListener
			? Test2AnyMultipleContextStandardEventListener
			: (Test2AnyMultipleContextStandardEventListenerComponent)CreateComponent(Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener, typeof(Test2AnyMultipleContextStandardEventListenerComponent));
		componentListener.Delegate += value;
		ReplaceComponent(Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener, componentListener);
		if(invokeOnSubscribe && HasComponent(Test2ComponentsLookup.MultipleContextStandardEvent))
		{
			var component = MultipleContextStandardEvent;
			value(this, component.value);
		}

		return new JCMG.EntitasRedux.Events.EventDisposable<OnTest2AnyMultipleContextStandardEvent>(CreationIndex, value, UnsubscribeAnyMultipleContextStandardEvent);
	}

	private void UnsubscribeAnyMultipleContextStandardEvent(int creationIndex, OnTest2AnyMultipleContextStandardEvent value)
	{
		if(!IsEnabled || CreationIndex != creationIndex)
			return;

		var index = Test2ComponentsLookup.Test2AnyMultipleContextStandardEventListener;
		var component = Test2AnyMultipleContextStandardEventListener;
		component.Delegate -= value;
		if (Test2AnyMultipleContextStandardEventListener.IsEmpty)
		{
			RemoveComponent(index);
		}
		else
		{
			ReplaceComponent(index, component);
		}
	}
}
