//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class Test2Entity
{
	public Test2AnyEventToGenerateListenerComponent Test2AnyEventToGenerateListener { get { return (Test2AnyEventToGenerateListenerComponent)GetComponent(Test2ComponentsLookup.Test2AnyEventToGenerateListener); } }
	public bool HasTest2AnyEventToGenerateListener { get { return HasComponent(Test2ComponentsLookup.Test2AnyEventToGenerateListener); } }

	public void AddTest2AnyEventToGenerateListener()
	{
		var index = Test2ComponentsLookup.Test2AnyEventToGenerateListener;
		var component = (Test2AnyEventToGenerateListenerComponent)CreateComponent(index, typeof(Test2AnyEventToGenerateListenerComponent));
		AddComponent(index, component);
	}

	public void ReplaceTest2AnyEventToGenerateListener()
	{
		ReplaceComponent(Test2ComponentsLookup.Test2AnyEventToGenerateListener, Test2AnyEventToGenerateListener);
	}

	public void RemoveTest2AnyEventToGenerateListener()
	{
		RemoveComponent(Test2ComponentsLookup.Test2AnyEventToGenerateListener);
	}
}

//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class Test2Matcher
{
	static JCMG.EntitasRedux.IMatcher<Test2Entity> _matcherTest2AnyEventToGenerateListener;

	public static JCMG.EntitasRedux.IMatcher<Test2Entity> Test2AnyEventToGenerateListener
	{
		get
		{
			if (_matcherTest2AnyEventToGenerateListener == null)
			{
				var matcher = (JCMG.EntitasRedux.Matcher<Test2Entity>)JCMG.EntitasRedux.Matcher<Test2Entity>.AllOf(Test2ComponentsLookup.Test2AnyEventToGenerateListener);
				matcher.ComponentNames = Test2ComponentsLookup.ComponentNames;
				_matcherTest2AnyEventToGenerateListener = matcher;
			}

			return _matcherTest2AnyEventToGenerateListener;
		}
	}
}

//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class Test2Entity
{
	public System.IDisposable SubscribeAnyEventToGenerate(OnTest2AnyEventToGenerate value, bool invokeOnSubscribe = true)
	{
		var componentListener = HasTest2AnyEventToGenerateListener
			? Test2AnyEventToGenerateListener
			: (Test2AnyEventToGenerateListenerComponent)CreateComponent(Test2ComponentsLookup.Test2AnyEventToGenerateListener, typeof(Test2AnyEventToGenerateListenerComponent));
		componentListener.Delegate += value;
		ReplaceComponent(Test2ComponentsLookup.Test2AnyEventToGenerateListener, componentListener);
		if(invokeOnSubscribe && HasComponent(Test2ComponentsLookup.EventToGenerate))
		{
			var component = EventToGenerate;
			value(this, component.value);
		}

		return new JCMG.EntitasRedux.Events.EventDisposable<OnTest2AnyEventToGenerate>(CreationIndex, value, UnsubscribeAnyEventToGenerate);
	}

	private void UnsubscribeAnyEventToGenerate(int creationIndex, OnTest2AnyEventToGenerate value)
	{
		if(!IsEnabled || CreationIndex != creationIndex)
			return;

		var index = Test2ComponentsLookup.Test2AnyEventToGenerateListener;
		var component = Test2AnyEventToGenerateListener;
		component.Delegate -= value;
		if (Test2AnyEventToGenerateListener.IsEmpty)
		{
			RemoveComponent(index);
		}
		else
		{
			ReplaceComponent(index, component);
		}
	}
}
