//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial interface INewCustomNameComponent2Entity
{
	NewCustomNameComponent2Component NewCustomNameComponent2 { get; }
	bool HasNewCustomNameComponent2 { get; }

	void AddNewCustomNameComponent2(EntitasRedux.Tests.CustomName newValue);
	void ReplaceNewCustomNameComponent2(EntitasRedux.Tests.CustomName newValue);
	void RemoveNewCustomNameComponent2();
}
