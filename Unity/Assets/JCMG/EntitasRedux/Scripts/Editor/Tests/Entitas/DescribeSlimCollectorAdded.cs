using JCMG.EntitasRedux;
using NUnit.Framework;

namespace EntitasRedux.Tests
{
	[TestFixture]
	public class DescribeSlimCollectorAdded : ADescribeICollector
	{
		protected override ICollector<MyTestEntity> CreateCollector(IContext<MyTestEntity> context)
			=> new SlimCollector<MyTestEntity>(context, MyTestComponentsLookup.ComponentA, EventType.Added);

		protected override MyTestEntity CreateEntity(IContext<MyTestEntity> context)
			=> context.CreateEntity();

		protected override void TriggerCollect(MyTestEntity entity) => entity.AddComponentA();

		protected override void RemoveTrigger(MyTestEntity entity) => entity.RemoveComponentA();
	}
}