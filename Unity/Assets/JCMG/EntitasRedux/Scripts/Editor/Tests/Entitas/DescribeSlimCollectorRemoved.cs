using JCMG.EntitasRedux;
using NUnit.Framework;

namespace EntitasRedux.Tests
{
	[TestFixture]
	public class DescribeSlimCollectorRemoved : ADescribeICollector
	{
		protected override ICollector<MyTestEntity> CreateCollector(IContext<MyTestEntity> context)
			=> new SlimCollector<MyTestEntity>(context, MyTestComponentsLookup.ComponentA, EventType.Removed);

		protected override MyTestEntity CreateEntity(IContext<MyTestEntity> context)
		{
			var entity = context.CreateEntity();
			entity.AddComponentA();
			return entity;
		}

		protected override void TriggerCollect(MyTestEntity entity) => entity.RemoveComponentA();

		protected override void RemoveTrigger(MyTestEntity entity) => entity.AddComponentA();
	}
}