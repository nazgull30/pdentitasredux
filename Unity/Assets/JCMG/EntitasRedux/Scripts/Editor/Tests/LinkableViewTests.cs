using System.Collections;
using JCMG.EntitasRedux.Core.Utils;
using JCMG.EntitasRedux.Core.View.Impls;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace EntitasRedux.Tests
{
	public class LinkableViewTests
	{
		private TestContext _context;

		[SetUp]
		public void SetUp()
		{
			_context = new TestContext();
		}

		[UnityTest]
		public IEnumerator CreateLinkableSubscribeOnEventAndUnlink()
		{
			var entity = _context.CreateEntity();
			var gameObject = new GameObject();
			var linkable = gameObject.AddComponent<TestLinkable>();

			yield return null;

			linkable.Link(entity);

			Assert.True(entity.HasFlagEntityEventListener, "entity.HasFlagEntityEventListener");

			linkable.Unlink();

			Assert.False(entity.HasFlagEntityEventListener, "entity.HasFlagEntityEventListener");
		}

		public class TestLinkable : LinkableView<TestEntity>
		{
			protected override void Subscribe(TestEntity entity, IUnsubscribeEvent unsubscribe)
			{
				entity.SubscribeFlagEntityEvent(Value).AddTo(unsubscribe);
			}

			private void Value(TestEntity entity)
			{
			}
		}
	}
}