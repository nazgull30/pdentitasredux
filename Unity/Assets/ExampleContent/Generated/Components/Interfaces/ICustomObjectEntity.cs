//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial interface ICustomObjectEntity
{
	ExampleContent.VisualDebugging.CustomObjectComponent CustomObject { get; }
	bool HasCustomObject { get; }

	void AddCustomObject(ExampleContent.VisualDebugging.CustomObject newCustomObject);
	void ReplaceCustomObject(ExampleContent.VisualDebugging.CustomObject newCustomObject);
	void RemoveCustomObject();
}
