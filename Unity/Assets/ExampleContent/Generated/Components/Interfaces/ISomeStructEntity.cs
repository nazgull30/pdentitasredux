//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial interface ISomeStructEntity
{
	SomeStructComponent SomeStruct { get; }
	bool HasSomeStruct { get; }

	void AddSomeStruct(ExampleContent.VisualDebugging.SomeStruct newValue);
	void ReplaceSomeStruct(ExampleContent.VisualDebugging.SomeStruct newValue);
	void RemoveSomeStruct();
}
