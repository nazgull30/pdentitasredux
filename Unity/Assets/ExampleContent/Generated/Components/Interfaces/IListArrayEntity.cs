//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial interface IListArrayEntity
{
	ExampleContent.VisualDebugging.ListArrayComponent ListArray { get; }
	bool HasListArray { get; }

	void AddListArray(System.Collections.Generic.List<string>[] newListArray);
	void ReplaceListArray(System.Collections.Generic.List<string>[] newListArray);
	void RemoveListArray();
}
