//------------------------------------------------------------------------------
// <auto-generated>
//		This code was generated by a tool (Genesis v2.3.2.0).
//
//
//		Changes to this file may cause incorrect behavior and will be lost if
//		the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial interface ISimpleObjectEntity
{
	ExampleContent.VisualDebugging.SimpleObjectComponent SimpleObject { get; }
	bool HasSimpleObject { get; }

	void AddSimpleObject(ExampleContent.VisualDebugging.SimpleObject newSimpleObject);
	void ReplaceSimpleObject(ExampleContent.VisualDebugging.SimpleObject newSimpleObject);
	void RemoveSimpleObject();
}
